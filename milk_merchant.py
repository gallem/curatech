"""
@Author Marco A. Gallegos
@Date   2021/01/05
@Update 2021/01/05 | Marco A. Gallegos
@Description
    This files contains functions to solve the MilkMerchant problem.
    It must return an integer representing the number of matching pairs 
    of socks that are available.
"""

import re


def milk_merchant(n:int, ar:str)->int:
    """Thjis function determines the number of
    matching pairs (same values) in the ar parameter,
    the strategy is simple sort and linear comparison

    Args:
        n (int): number of milk boxes ,0 < n <=100
        ar (str): color of the boxes

    Returns:
        int: number of pairs
    """
    pattern = re.compile(r'[\d ]+[\d]?')
    match = re.fullmatch(pattern, ar)

    if not match:
        raise Exception('invalid milk boxes')

    ar = ar.split(" ")
    ar = [int(a) for a in ar]
    
    len_ar = len(ar)
    if n>100 or n < 1 or len_ar > n :
        raise Exception("Invalid Arguments")

    ar.sort()

    if ar[0] < 1 or ar[len_ar-1] >100:
        raise Exception("numbers out of range")

    matching_pairs = 0
    i = 1
    while i<len_ar:
        if ar[i] == ar[i-1]:
            matching_pairs += 1
            i += 2
        else:
            i += 1


    return matching_pairs


if __name__ == '__main__':
    print(milk_merchant(n=9,ar="10 20 20 10 10 30 50 10 20"))
    print(milk_merchant(n=20,ar="4 5 5 5 6 6 4 1 4 4 3 6 6 3 6 1 4 5 5 5"))