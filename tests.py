"""
@Author Marco A. Gallegos
@Date   2021/01/06
@Update 2021/01/06 | Marco A. Gallegos
@Description
    This files contains unit tests for thhe 3 problems (milk merchant, valley count, fizzbuzz).
"""
import unittest
from counting_valleys import counting_valleys
from milk_merchant import milk_merchant
from fizzbuzz import type_of_number


class ExersiceTests(unittest.TestCase):

    def test_milk_good_example(self):
        self.assertEqual(milk_merchant(n=20,ar="4 5 5 5 6 6 4 1 4 4 3 6 6 3 6 1 4 5 5 5"), 9, "Should be 9")
        
    def test_milk_merchant_bad_ar(self):
        result=False
        try:
            milk_merchant(n=20, ar="4 5 5 5 6 6 4 1 4 4 3 6 6 3 6 1 4 5 5 a")
        except Exception as e:
            if str(e) == 'invalid milk boxes':
                result=True
        self.assertTrue(result)
    
    def test_milk_merchant_bad_n(self):
        result=False
        try:
            milk_merchant(n=19, ar="4 5 5 5 6 6 4 1 4 4 3 6 6 3 6 1 4 5 5 5")
        except Exception as e:
            if str(e) == 'Invalid Arguments':
                result=True
        self.assertTrue(result)
    
    def test_milk_merchant_out_of_range(self):
        result=False
        try:
            milk_merchant(n=19, ar="4 5 5 5 6 6 4 1 4 4 3 6 6 3 6 1 4 555")
        except Exception as e:
            if str(e) == 'numbers out of range':
                result=True
        self.assertTrue(result)

    def test_fizzbuzz_3(self):
        self.assertEqual("Fizz", type_of_number(3), "Should by Fizz")
    
    def test_fizzbuzz_5(self):
        self.assertEqual("Buzz", type_of_number(5), "Should by Buzz")
    
    def test_fizzbuzz_fizzbuzzz(self):
        self.assertEqual("FizzBuzz", type_of_number(90), "Should by FizzBuzz")
    
    def test_counting_valleys_good_example(self):
        self.assertEqual(counting_valleys(steps=12, path="DDUUDDUDUUUD"), 2, "Should be 2")
    
    def test_counting_valleys_bad_path(self):
        result=False
        try:
            counting_valleys(steps=12, path="UUUUDDUDUUUDE")
        except Exception as e:
            if str(e) == 'invalid path':
                result=True
        self.assertTrue(result)
    
    def test_counting_valleys_steps_mistmatch(self):
        result=False
        try:
            counting_valleys(steps=13, path="UUUUDDUDUUUD")
        except Exception as e:
            if str(e) == 'steps and path mistmatch':
                result=True
        self.assertTrue(result)

if __name__ == '__main__':
    unittest.main()
