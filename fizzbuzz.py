"""
@Author Marco A. Gallegos
@Date   2021/01/05
@Update 2021/01/05 | Marco A. Gallegos
@Description
    This files contains functions to solve the FizzBuzz problem.
"""


def type_of_number(number:int)->int:
    """this function determines the type of print
    of a number, considering only this particular translations:
     3 * n = Fizz
     5 * n = BUZZ
     (3 * n and 5 * n) = FizzBuzz

    Args:
        number (int): number to determinate translation

    Returns:
        int | str : translation 
    """
    type_1 = number%3
    type_2 = number%5
    if type_1 == 0 and type_2 == 0:
        return "FizzBuzz"
    elif type_2 == 0:
        return "Buzz"
    elif type_1 == 0:
        return "Fizz"
    else:
        return number


def fizz_buzz(translator=type_of_number):
    """this function prints all the numbers from 1 to 1000 
    using the result of the function in the parameters. 
    
    Args:
        translator (function): a custom function to tranlate the number
    """
    for number in range(1,101):
        print(translator(number))


if __name__ == "__main__":
    fizz_buzz()