"""
@Author Marco A. Gallegos
@Date   2021/01/05
@Update 2021/01/05 | Marco A. Gallegos
@Description
    This files contains functions to solve the counting valleys problem.
"""
import re

def counting_valleys(steps:int, path:str)->int:
    """this function determines the number of Valleys
    crossed on the path

    Args:
        steps (int): number of steps
        path (str): step description

    Returns:
        int: number of valleys crossed
    """
    valleys = 0
    current_level = 0
    can_add_valley = True

    pattern = re.compile(r'[D|U]{2,}')
    match = re.fullmatch(pattern, path)

    if not match:
        raise Exception('invalid path')
    if steps < 2 or steps > 10**6:
        raise Exception('steps out of range')
    if len(path) != steps:
        raise Exception('steps and path mistmatch')
    
    for step in path:
        if step == 'D':
            current_level -= 1
        elif step == 'U':
            current_level += 1
        
        if current_level<0 and can_add_valley:
            valleys += 1
            can_add_valley=False
        elif current_level>-1 and not can_add_valley:
            can_add_valley=True

    return valleys



if __name__ == "__main__":
    print(counting_valleys(steps=12, path="DDUUDDUDUUUD"))
    print(counting_valleys(steps=8, path="UDDDUDUU"))